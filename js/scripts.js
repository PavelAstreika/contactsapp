class User {

    #data = {}

    constructor({id, name, email, address, phone}) {
        this.#data = {
            id,
            name,
            email,
            address,
            phone
        }
    }

    set userData (newData) {
        this.#data = {...newData}
    }

    get userData () {
        return this.#data
    }
}

class Contacts {
    constructor () {
        this.contactsData = [];
    }

    add ({name, email, address, phone}) {
        const contactsUser = new User({
            id: new Date().getTime().toString(),
            name,
            email,
            address,
            phone
        })
        this.contactsData.push(contactsUser)
    }

    edit(userId, updateUserData) {
        this.contactsData = this.contactsData.map((user) => {
            if(user.userData.id === userId) {
                user.userData = {
                    id: userId,
                    ...updateUserData
                }
                // user.userData = {
                //     ...user['userData'],
                //     ...updateUserData
                // }
            }
            return user
        }) 
    }

    remove (userId) {
        this.contactsData = this.contactsData.filter((user) => user.userData.id !== userId)
    }

    get () {
        return this.contactsData
    }
    
}

class ContactsApp extends Contacts {

    constructor () {
        super();
        this.checkCookieForLocalStorage();
        this.contactsData = this.loadedLocalStorageData() || [];
        this.create();
        this.addEventForButtonAdd();
    }

    loadedLocalStorageData () {
        const localStorageData = JSON.parse(this.storage);
        if(!localStorageData) {
            return;
        }
        const data = localStorageData.map(item => new User(item))
        return data;
    }

    get storage () {
        return localStorage.getItem('contacts');
    }

    set storage (data) {
        const contacts = data.map(item =>item.userData);
        this.storageExpiration();
        localStorage.setItem('contacts', JSON.stringify(contacts));
    }

    findCookie(name) {
        let matches = document.cookie.match(new RegExp(
              "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
            ));
            return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    checkCookieForLocalStorage() { 
        if(!this.findCookie('user')) {
            localStorage.clear()
        }
    }


    storageExpiration() { 
        document.cookie = 'user=contacts; max-age=60';
    }  

    addEventForButtonAdd () {
        const addButton = document.querySelector('.contact__button')
        addButton.addEventListener('click', () => {
            const inputName = document.querySelector('.contact__name');
            const inputTel = document.querySelector('.contact__phone');
            const inputEmail = document.querySelector('.contact__email');
            const inputAddress = document.querySelector('.contact__address');
            this.onAdd({
                name: inputName.value,
                phone: inputTel.value,
                email: inputEmail.value,
                address: inputAddress.value
            })
            inputName.value = '';
            inputTel.value = '';
            inputEmail.value = '';
            inputAddress.value = '';
        })
    }

    create () {
        const app = document.querySelector('.app');
       
        if(!app) {
            return;
        }

        const contactHtml = this.createHtml();
        app.appendChild(contactHtml);
        this.render();
    }

    createHtml () {
        const contactsElement = document.createElement('div');
        contactsElement.classList.add('contacts');
        contactsElement.innerHTML = `<div class="contact__header">
                                        <input type="text" class="contact__name" placeholder="Имя">
                                        <input type="tel" class="contact__phone" placeholder="Номер телефона">
                                        <input type="email" class="contact__email" placeholder="Email">
                                        <input type="text" class="contact__address" placeholder="Адрес">
                                    </div>
                                    <button class="contact__button">Добавить</button>
                                    <div class="contact__content">
                                    </div>`
        return contactsElement

    }

    onAdd (contact) {
        this.add(contact);
        this.storage = this.contactsData;
        this.render();
    }

    onRemove (contactId) {
        this.remove(contactId);
        this.storage = this.contactsData;
        this.render();
    }

    addEventDeleteButtons () {
        const deleteBtns = document.querySelectorAll('.delete__button');
        deleteBtns.forEach((deleteBtn) => {
            deleteBtn.addEventListener('click', (e) => {
                this.onRemove(e.target.id)
            })
        })
    }

    render () {
        const contentElement = document.querySelector('.contact__content');

        contentElement.innerHTML = '';

        if(this.contactsData.length === 0) {
            contentElement.innerHTML = '<h3 class="content__empty">Нет данных</h3>';
            return;
        }

        const contactItemList = document.createElement('ul');
        contactItemList.classList.add('contacts__itemes');

        let contactsItems = '';

        this.contactsData.forEach((item) => {
            const data = item.userData
            const {name, address, phone, id, email} = data
            contactsItems += `<li class="contacts__item">
                                <div class="name"><span class="bold">Имя:</span> ${name}</div>
                                <div class="phone"><span class="bold">Телефон:</span> ${phone}</div>
                                <div class="address"><span class="bold">Адрес:</span> ${address}</div>
                                <div class="email"><span class="bold">Email:</span> ${email}</div>
                                <button class="delete__button" id="${id}">Удалить</button>
                            </li>`
        
        })

        contactItemList.innerHTML = contactsItems;
        contentElement.appendChild(contactItemList);
        this.addEventDeleteButtons();
    }

}

const app = new ContactsApp();



